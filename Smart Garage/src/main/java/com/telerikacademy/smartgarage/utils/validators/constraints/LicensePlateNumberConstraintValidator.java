package com.telerikacademy.smartgarage.utils.validators.constraints;

import com.telerikacademy.smartgarage.exceptions.InvalidUserInputException;
import com.telerikacademy.smartgarage.utils.validators.LicensePlateValidator;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.messageinterpolation.ExpressionLanguageFeatureLevel;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LicensePlateNumberConstraintValidator implements ConstraintValidator<LicensePlateNumber, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            LicensePlateValidator.validateLicensePlateNumber(value);
        } catch (InvalidUserInputException e) {
            HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);

            hibernateContext.disableDefaultConstraintViolation();
            hibernateContext.addExpressionVariable("message", e.getMessage())
                    .buildConstraintViolationWithTemplate("${message}")
                    .enableExpressionLanguage(ExpressionLanguageFeatureLevel.BEAN_PROPERTIES)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    @Override
    public void initialize(LicensePlateNumber constraintAnnotation) {
//        ConstraintValidator.super.initialize(constraintAnnotation);
    }
}