package com.telerikacademy.smartgarage.utils.validators;

import com.telerikacademy.smartgarage.exceptions.InvalidUserInputException;
import com.telerikacademy.smartgarage.models.enums.BulgarianLicensePlateDetails;

import java.util.List;

public class LicensePlateValidator {

    public static void validateLicensePlateNumber(String licensePlateNumber) {
        int length = licensePlateNumber.length();
        validateLength(length);
        String provinceCode = extractProvinceCode(licensePlateNumber, length);
        validateProvinceCode(provinceCode);
        String serialNumber = extractSerialNumber(licensePlateNumber, length);
        validateSerialNumber(serialNumber);
        String series = extractSeries(licensePlateNumber, length);
        validateSeries(series);
    }

    private static void validateLength(int length) {
        if (length < 7 || length > 8) {
            throw new InvalidUserInputException("License plate number must be between 7 and 8 symbols.");
        }
    }

    private static String extractProvinceCode(String licensePlateNumber, int length) {
        if (length == 7) return licensePlateNumber.substring(0, 1);
        else return licensePlateNumber.substring(0, 2).toUpperCase();
    }

    private static void validateProvinceCode(String provinceCode) {
        if (BulgarianLicensePlateDetails.getByProvinceCode(provinceCode) == null) {
            throw new InvalidUserInputException("Invalid license plate province code");
        }
    }

    private static String extractSerialNumber(String licensePlateNumber, int length) {
        if (length == 7) return licensePlateNumber.substring(1, 5);
        else return licensePlateNumber.substring(2, 6);
    }

    private static void validateSerialNumber(String serialNumber) {
        for (char character : serialNumber.toCharArray()) {
            if (!Character.isDigit(character)) {
                throw new InvalidUserInputException("Invalid license plate serial number, must contain digits only.");
            }
        }
    }

    private static String extractSeries(String licensePlateNumber, int length) {
        if (length == 7) return licensePlateNumber.substring(5);
        else return licensePlateNumber.substring(6).toUpperCase();
    }

    private static void validateSeries(String series) {
        List<Character> validSeriesLetters = BulgarianLicensePlateDetails.getValidSeriesLetters();
        for (char letter : series.toCharArray()) {
            boolean isValid = false;
            for (char validLetter : validSeriesLetters) {
                if (letter == validLetter) {
                    isValid = true;
                    break;
                }
            }
            if (!isValid) {
                throw new InvalidUserInputException("Invalid license plate series.");
            }
        }
    }

}