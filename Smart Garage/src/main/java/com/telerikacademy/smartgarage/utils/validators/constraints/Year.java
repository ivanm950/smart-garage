package com.telerikacademy.smartgarage.utils.validators.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = YearConstraintValidator.class)
public @interface Year {

    String message() default "Invalid year";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}