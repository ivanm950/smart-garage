package com.telerikacademy.smartgarage.utils.validators.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LicensePlateNumberConstraintValidator.class)
public @interface LicensePlateNumber {

    String message() default "Invalid license plate number";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}