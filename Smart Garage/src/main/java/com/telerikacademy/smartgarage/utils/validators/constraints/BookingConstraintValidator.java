package com.telerikacademy.smartgarage.utils.validators.constraints;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class BookingConstraintValidator implements ConstraintValidator<Booking, LocalDateTime> {
    @Override
    public void initialize(Booking constraintAnnotation) {
//        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(LocalDateTime value, ConstraintValidatorContext context) {
        LocalDateTime minDate = LocalDateTime.now();
        LocalDateTime maxDate = LocalDateTime.now().plusMonths(1);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("d MMMM maxDate");
        if (value.isBefore(minDate) || value.isAfter(maxDate)){
            HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);

            hibernateContext.disableDefaultConstraintViolation();
            hibernateContext.addExpressionVariable("minDate", df.format(minDate))
                    .addExpressionVariable("maxDate", df.format(maxDate))
                    .buildConstraintViolationWithTemplate("Drop off date must be between ${minDate} and ${maxDate}")
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}