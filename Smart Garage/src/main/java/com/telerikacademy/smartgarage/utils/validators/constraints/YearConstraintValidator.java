package com.telerikacademy.smartgarage.utils.validators.constraints;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.messageinterpolation.ExpressionLanguageFeatureLevel;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class YearConstraintValidator implements ConstraintValidator<Year, LocalDate> {
    @Override
    public void initialize(Year constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        LocalDate minYear = LocalDate.of(1886,1,1);
        LocalDate currentYear = LocalDate.now();
        if (value.isBefore(minYear) || value.isAfter(currentYear)){
            HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);

            hibernateContext.disableDefaultConstraintViolation();
            hibernateContext.addExpressionVariable("minYear", minYear.toString())
                    .addExpressionVariable("currentYear", currentYear.toString())
                    .buildConstraintViolationWithTemplate("Year of creation must be between ${minYear} and ${currentYear}")
                    .enableExpressionLanguage(ExpressionLanguageFeatureLevel.BEAN_PROPERTIES)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}