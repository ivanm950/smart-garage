package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.user.UserRole;
import com.telerikacademy.smartgarage.repositories.contracts.UserRoleRepository;
import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public List<UserRole> getAll() {
        return userRoleRepository.getAll();
    }

    @Override
    public UserRole getById(int id) {
        return userRoleRepository.getById(id);
    }

}