package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<com.telerikacademy.smartgarage.models.service.Service> getAll() {
        return serviceRepository.getAll();
    }

    @Override
    public com.telerikacademy.smartgarage.models.service.Service getById(int id) {
        return serviceRepository.getById(id);
    }

    @Override
    public <V> Optional<com.telerikacademy.smartgarage.models.service.Service> getByFieldMatches(String fieldName, V value) {
        return serviceRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<com.telerikacademy.smartgarage.models.service.Service> getAllByFieldMatches(String fieldName, V value) {
        return serviceRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<com.telerikacademy.smartgarage.models.service.Service> getByFieldContains(String fieldName, V value) {
        return serviceRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<com.telerikacademy.smartgarage.models.service.Service> getAllByFieldContains(String fieldName, V value) {
        return serviceRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(com.telerikacademy.smartgarage.models.service.Service entity) {
        serviceRepository.create(entity);
    }

    @Override
    public void update(com.telerikacademy.smartgarage.models.service.Service entity) {
        serviceRepository.update(entity);
    }

    @Override
    public void delete(com.telerikacademy.smartgarage.models.service.Service service) {
        serviceRepository.delete(service);
    }

}