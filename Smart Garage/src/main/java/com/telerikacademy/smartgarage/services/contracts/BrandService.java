package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.brand.Brand;

import java.util.List;
import java.util.Optional;

public interface BrandService {

    List<Brand> getAll();

    Brand getById(int id);

    <V> Optional<Brand> getByFieldMatches(String fieldName, V value);

    <V> List<Brand> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Brand> getByFieldContains(String fieldName, V value);

    <V> List<Brand> getAllByFieldContains(String fieldName, V value);

    void create(Brand entity);

    void update(Brand entity);

    void delete(Brand entity);

}