package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.service.Service;

import java.util.List;
import java.util.Optional;

public interface ServiceService {

    List<Service> getAll();

    Service getById(int id);

    <V> Optional<Service> getByFieldMatches(String fieldName, V value);

    <V> List<Service> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Service> getByFieldContains(String fieldName, V value);

    <V> List<Service> getAllByFieldContains(String fieldName, V value);

    void create(Service entity);

    void update(Service entity);

    void delete(Service entity);

}