package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.user.User;

import java.util.List;
import java.util.Optional;

public interface ServiceCategoryService {

    List<ServiceCategory> getAll();

    ServiceCategory getById(int id);

    <V> Optional<ServiceCategory> getByFieldMatches(String fieldName, V value);

    <V> List<ServiceCategory> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<ServiceCategory> getByFieldContains(String fieldName, V value);

    <V> List<ServiceCategory> getAllByFieldContains(String fieldName, V value);

    void create(ServiceCategory entity, User requester);

    void update(ServiceCategory entity, User requester);

    void delete(ServiceCategory entity, User requester);

}