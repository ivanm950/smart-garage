package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.model.Model;

import java.util.List;
import java.util.Optional;

public interface ModelService {

    List<Model> getAll();

    Model getById(int id);

    <V> Optional<Model> getByFieldMatches(String fieldName, V value);

    <V> List<Model> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Model> getByFieldContains(String fieldName, V value);

    <V> List<Model> getAllByFieldContains(String fieldName, V value);

    void create(Model entity);

    void update(Model entity);

    void delete(Model model);

}