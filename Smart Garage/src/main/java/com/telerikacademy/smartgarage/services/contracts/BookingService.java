package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.booking.Booking;

import java.util.List;
import java.util.Optional;

public interface BookingService {

    List<Booking> getAll();

    Booking getById(int id);

    <V> Optional<Booking> getByFieldMatches(String fieldName, V value);

    <V> List<Booking> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Booking> getByFieldContains(String fieldName, V value);

    <V> List<Booking> getAllByFieldContains(String fieldName, V value);

    void create(Booking entity);

    void update(Booking entity);

    void delete(Booking entity, User requester);

}