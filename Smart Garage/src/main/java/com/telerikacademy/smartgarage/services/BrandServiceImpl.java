package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    @Autowired
    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Brand> getAll() {
        return brandRepository.getAll();
    }

    @Override
    public Brand getById(int id) {
        return brandRepository.getById(id);
    }

    @Override
    public <V> Optional<Brand> getByFieldMatches(String fieldName, V value) {
        return brandRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<Brand> getAllByFieldMatches(String fieldName, V value) {
        return brandRepository.getAllByFieldMatches(fieldName,value);
    }

    @Override
    public <V> Optional<Brand> getByFieldContains(String fieldName, V value) {
        return brandRepository.getByFieldContains(fieldName,value);
    }

    @Override
    public <V> List<Brand> getAllByFieldContains(String fieldName, V value) {
        return brandRepository.getAllByFieldContains(fieldName,value);
    }

    @Override
    public void create(Brand entity) {
        boolean vehicleExist = true;
        try {
            brandRepository.getByName(entity.getName());
        }catch (EntityNotFoundException e){
            vehicleExist = false;
        }
        if(vehicleExist){
            throw new DuplicateEntityException("Brand","name",entity.getName());
        }
        brandRepository.create(entity);
    }

    @Override
    public void update(Brand entity) {
        boolean duplicateExists = true;
        try {
            Brand existingBrand = brandRepository.getByName(entity.getName());
            if (existingBrand.getId() == entity.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) throw new DuplicateEntityException("Brand", "vin", entity.getName());

        brandRepository.update(entity);
    }

    @Override
    public void delete(Brand brand) {
        brandRepository.delete(brand);
    }

}