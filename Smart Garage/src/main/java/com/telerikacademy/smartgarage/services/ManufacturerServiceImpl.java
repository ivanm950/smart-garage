package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.smartgarage.services.contracts.ManufacturerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;

    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public List<Manufacturer> getAll() {
        return manufacturerRepository.getAll();
    }

    @Override
    public Manufacturer getById(int id) {
        return manufacturerRepository.getById(id);
    }

    @Override
    public <V> Optional<Manufacturer> getByFieldMatches(String fieldName, V value) {
        return manufacturerRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<Manufacturer> getAllByFieldMatches(String fieldName, V value) {
        return manufacturerRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<Manufacturer> getByFieldContains(String fieldName, V value) {
        return manufacturerRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<Manufacturer> getAllByFieldContains(String fieldName, V value) {
        return manufacturerRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(Manufacturer entity) {
        manufacturerRepository.create(entity);
    }

    @Override
    public void update(Manufacturer entity) {
        manufacturerRepository.update(entity);
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        manufacturerRepository.delete(manufacturer);
    }

}