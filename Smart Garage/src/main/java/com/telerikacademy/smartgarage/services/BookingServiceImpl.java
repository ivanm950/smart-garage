package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.repositories.contracts.BookingRepository;
import com.telerikacademy.smartgarage.services.contracts.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;

    @Autowired
    public BookingServiceImpl(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public List<Booking> getAll() {
        return bookingRepository.getAll();
    }

    @Override
    public Booking getById(int id) {
        return bookingRepository.getById(id);
    }

    @Override
    public <V> Optional<Booking> getByFieldMatches(String fieldName, V value) {
        return bookingRepository.getByFieldMatches(fieldName, value);
    }

    @Override
    public <V> List<Booking> getAllByFieldMatches(String fieldName, V value) {
        return bookingRepository.getAllByFieldMatches(fieldName, value);
    }

    @Override
    public <V> Optional<Booking> getByFieldContains(String fieldName, V value) {
        return bookingRepository.getByFieldContains(fieldName, value);
    }

    @Override
    public <V> List<Booking> getAllByFieldContains(String fieldName, V value) {
        return bookingRepository.getAllByFieldContains(fieldName, value);
    }

    @Override
    public void create(Booking entity) {
        bookingRepository.create(entity);
    }

    @Override
    public void update(Booking entity) {
        bookingRepository.update(entity);
    }

    @Override
    public void delete(Booking booking, User requester) {
        if (!requester.isEmployee() || requester.getId() != booking.getVehicle().getOwner().getId()) {
            throw new UnauthorisedOperationException("You're not authorised to delete this booking");
        }
        bookingRepository.delete(booking);
    }

}