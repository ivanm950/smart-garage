package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Manufacturer;

import java.util.List;
import java.util.Optional;

public interface ManufacturerService {

    List<Manufacturer> getAll();

    Manufacturer getById(int id);

    <V> Optional<Manufacturer> getByFieldMatches(String fieldName, V value);

    <V> List<Manufacturer> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Manufacturer> getByFieldContains(String fieldName, V value);

    <V> List<Manufacturer> getAllByFieldContains(String fieldName, V value);

    void create(Manufacturer entity);

    void update(Manufacturer entity);

    void delete(Manufacturer entity);

}