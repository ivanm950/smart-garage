package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.vehicle.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {

    List<Vehicle> getAll();

    Vehicle getById(int id);

    <V> Optional<Vehicle> getByFieldMatches(String fieldName, V value);

    <V> List<Vehicle> getAllByFieldMatches(String fieldName, V value);

    <V> Optional<Vehicle> getByFieldContains(String fieldName, V value);

    <V> List<Vehicle> getAllByFieldContains(String fieldName, V value);

    void create(Vehicle entity);

    void update(Vehicle entity);

    void delete(Vehicle entity);

}