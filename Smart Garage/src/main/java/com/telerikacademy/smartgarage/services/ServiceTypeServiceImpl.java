package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceTypeRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceTypeServiceImpl implements ServiceTypeService {

    private final ServiceTypeRepository serviceTypeRepository;

    @Autowired
    public ServiceTypeServiceImpl(ServiceTypeRepository serviceTypeRepository) {
        this.serviceTypeRepository = serviceTypeRepository;
    }

    @Override
    public List<ServiceType> getAll() {
        return serviceTypeRepository.getAll();
    }

    @Override
    public ServiceType getById(int id) {
        return serviceTypeRepository.getById(id);
    }

    @Override
    public <V> Optional<ServiceType> getByFieldMatches(String fieldName, V value) {
        return serviceTypeRepository.getByFieldMatches(fieldName,value);
    }

    @Override
    public <V> List<ServiceType> getAllByFieldMatches(String fieldName, V value) {
        return serviceTypeRepository.getAllByFieldMatches(fieldName,value);
    }

    @Override
    public <V> Optional<ServiceType> getByFieldContains(String fieldName, V value) {
        return serviceTypeRepository.getByFieldContains(fieldName,value);
    }

    @Override
    public <V> List<ServiceType> getAllByFieldContains(String fieldName, V value) {
        return serviceTypeRepository.getAllByFieldContains(fieldName,value);
    }

    @Override
    public void create(ServiceType entity, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to create services");
        serviceTypeRepository.create(entity);
    }

    @Override
    public void update(ServiceType entity, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to update services");
        serviceTypeRepository.update(entity);
    }

    @Override
    public void delete(ServiceType serviceType, User requester) {
        if (!requester.isEmployee()) throw new UnauthorisedOperationException("You're not authorised to delete services");
        serviceTypeRepository.delete(serviceType);
    }

}