package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.user.UserRole;

import java.util.List;

public interface UserRoleService {

    List<UserRole> getAll();

    UserRole getById(int id);

}