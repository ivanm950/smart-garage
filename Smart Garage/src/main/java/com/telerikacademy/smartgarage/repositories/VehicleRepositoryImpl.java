package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VehicleRepositoryImpl extends BaseModifyRepositoryImpl<Vehicle> implements VehicleRepository {

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(Vehicle.class, sessionFactory);
    }

    @Override
    public Vehicle  getByVin(String vin) {
        return getByFieldContains("vin", vin)
                .orElseThrow(() -> new EntityNotFoundException("Vehicle", "vin", vin));
    }

}