package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.BookingRepository;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookingRepositoryImpl extends BaseModifyRepositoryImpl<Booking> implements BookingRepository {

    @Autowired
    public BookingRepositoryImpl(SessionFactory sessionFactory) {
        super(Booking.class, sessionFactory);
    }

}