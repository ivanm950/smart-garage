package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ModelRepositoryImpl extends BaseModifyRepositoryImpl<Model> implements ModelRepository {

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(Model.class, sessionFactory);
    }

}