package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ManufacturerRepositoryImpl extends BaseModifyRepositoryImpl<Manufacturer> implements ManufacturerRepository {

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        super(Manufacturer.class, sessionFactory);
    }

}