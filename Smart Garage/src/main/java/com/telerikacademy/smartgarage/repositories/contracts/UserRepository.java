package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.user.User;

public interface UserRepository extends BaseModifyRepository<User>{

    User getByUsername(String username);

    User getByEmail(String email);

    User findByResetPasswordToken(String token);

}