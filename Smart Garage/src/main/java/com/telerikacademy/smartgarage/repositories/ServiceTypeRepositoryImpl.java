package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceTypeRepositoryImpl extends BaseModifyRepositoryImpl<ServiceType> implements ServiceTypeRepository {

    @Autowired
    public ServiceTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(ServiceType.class, sessionFactory);
    }

}