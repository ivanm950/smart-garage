package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.booking.Booking;

public interface BookingRepository extends BaseModifyRepository<Booking>{

}