package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceRepositoryImpl extends BaseModifyRepositoryImpl<Service> implements ServiceRepository {

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(Service.class, sessionFactory);
    }

}