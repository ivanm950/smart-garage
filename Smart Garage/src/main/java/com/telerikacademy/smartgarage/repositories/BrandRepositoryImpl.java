package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.AbstractRabbitListenerContainerFactoryConfigurer;
import org.springframework.stereotype.Repository;

@Repository
public class BrandRepositoryImpl extends BaseModifyRepositoryImpl<Brand> implements BrandRepository {

    @Autowired
    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        super(Brand.class, sessionFactory);
    }

    @Override
    public Brand getByName(String name) {
        return getByFieldMatches("name", name)
                .orElseThrow(() -> new EntityNotFoundException("Brand", "name", name));
    }

}