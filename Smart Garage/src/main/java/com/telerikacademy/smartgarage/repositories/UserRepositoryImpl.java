package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public User getByUsername(String username) {
        return getByFieldMatches("username", username)
                .orElseThrow(() -> new EntityNotFoundException("User", "username", username));
    }

    @Override
    public User getByEmail(String email) {
        return getByFieldMatches("email", email)
                .orElseThrow(() -> new EntityNotFoundException("Email", "email", email));
    }

    @Override
    public User findByResetPasswordToken(String token) {
        return getByFieldMatches("resetPasswordToken",token)
                .orElseThrow(() -> new EntityNotFoundException("ResetPasswordToken","resetPasswordToken",token));
    }


}