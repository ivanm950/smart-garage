package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.user.UserRole;
import com.telerikacademy.smartgarage.repositories.contracts.UserRoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleRepositoryImpl extends BaseGetRepositoryImpl<UserRole> implements UserRoleRepository {

    @Autowired
    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
    }

}