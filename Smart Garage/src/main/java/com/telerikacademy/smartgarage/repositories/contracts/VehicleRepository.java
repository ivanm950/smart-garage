package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.vehicle.Vehicle;

public interface VehicleRepository extends BaseModifyRepository<Vehicle> {

    Vehicle getByVin(String vin);

}