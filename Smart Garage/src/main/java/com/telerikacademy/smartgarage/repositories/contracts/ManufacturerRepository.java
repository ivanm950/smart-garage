package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Manufacturer;

public interface ManufacturerRepository extends BaseModifyRepository<Manufacturer> {



}