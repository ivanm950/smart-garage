package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.brand.Brand;

public interface BrandRepository extends BaseModifyRepository<Brand> {

    Brand getByName(String username);

}