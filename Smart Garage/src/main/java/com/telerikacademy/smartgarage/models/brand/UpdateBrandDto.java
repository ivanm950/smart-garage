package com.telerikacademy.smartgarage.models.brand;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateBrandDto {

    @NotNull
    @Size(min = 3,max = 14,message = "Brand name should be between 3 and 14 symbols")
    public String name;

    public UpdateBrandDto(String name) {
        this.name = name;
    }

    public UpdateBrandDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
