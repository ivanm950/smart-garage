package com.telerikacademy.smartgarage.models.service;

import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.booking.Booking;

import javax.persistence.*;

@Entity
@Table(name = "bookings_services")
public class Service {

    // TODO: 3/31/2022 VANKATA

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;

    @OneToOne
    @JoinColumn(name = "service_type_id")
    ServiceType serviceType;

    @Column(name = "price")
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}