package com.telerikacademy.smartgarage.models.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CreateModelDto {

    @NotNull
    @Size(min = 2,max = 10,message = "Model must be between 2 and 10 symbols")
    private String name;

    @NotNull
    private int brandId;


    public CreateModelDto(String name) {
        this.name = name;
    }

    public CreateModelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}
