package com.telerikacademy.smartgarage.models.service;

import com.telerikacademy.smartgarage.models.servicetype.ServiceType;

import javax.validation.constraints.Positive;

public class ServiceDTO {

    @Positive
    private double price;

    private ServiceType serviceType;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }
}