package com.telerikacademy.smartgarage.models.user;

import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;
    private final UserRoleService userRoleService;

    @Autowired
    public UserMapper(UserService userService, UserRoleService userRoleService) {
        this.userService = userService;
        this.userRoleService = userRoleService;
    }

    public User createDtoToObject(CreateUserDTO userDTO) {
        User user = new User();

        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        UserRole role = userRoleService.getById(1);
        user.setUserRole(role);

        return user;
    }

    public User updateDtoToObject(UpdateUserDTO userDTO, User userToUpdate) {
        User user = new User();

        user.setId(userToUpdate.getId());

        if (userDTO.getUsername() != null) user.setUsername(userDTO.getUsername());
        else user.setUsername(userToUpdate.getUsername());

        if (userDTO.getEmail() != null) user.setEmail(userDTO.getEmail());
        else user.setEmail(userToUpdate.getEmail());

        if (userDTO.getPassword() != null) user.setPassword(userDTO.getPassword());
        else user.setPassword(userToUpdate.getPassword());

        if (userDTO.getPhoneNumber() != null) user.setPhoneNumber(userDTO.getPhoneNumber());
        else user.setPhoneNumber(userToUpdate.getPhoneNumber());

//        if(userDTO.getPhoneNumber() != null) user.setPhoneNumber(userDTO.getPhoneNumber());

        if (userDTO.getRoleId() != 0) {
            UserRole userRole = userRoleService.getById(userDTO.getRoleId());
            user.setUserRole(userRole);
        } else user.setUserRole(userToUpdate.getUserRole());

        return user;
    }
}
