package com.telerikacademy.smartgarage.models.enums;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BulgarianLicensePlateDetails {

    //province codes
    BURGAS("A", "Burgas Province"),
    VARNA("B", "Varna Province"),
    VIDIN("BH", "Vidin Province"),
    VRATSA("BP", "Vratsa Province"),
    VELIKO_TARNOVO("BT", "Veliko Tarnovo Province"),
    BLAGOEVGRAD("E", "Blagoevgrad Province"),
    GABROVO("EB", "Gabrovo Province"),
    PLEVEN("EH", "Pleven Province"),
    KARDZHALI("K", "Kardzhali Province"),
    KYUSTENDIL("KH", "Kyustendil Province"),
    MONTANA("M", "Montana Province"),
    SHUMEN("H", "Shumen Province"),
    LOVECH("OB", "Lovech Province"),
    RUSE("P", "Ruse Province"),
    PAZARDZHIK("PA", "Pazardzhik Province"),
    PLOVDIV("PB", "Plovdiv Province"),
    PERNIK("PK", "Pernik Province"),
    RAZGRAD("PP", "Razgrad Province"),
    SOFIA_1("C", "Sofia City)"),
    SOFIA_2("CA", "Sofia City)"),
    SOFIA_3("CB", "Sofia City)"),
    SOFIA_4("CO", "Sofia Province"),
    SLIVEN("CH", "Sliven Province"),
    SMOLYAN("CM", "Smolyan Province"),
    SILISTRA("CC", "Silistra Province"),
    STARA_ZAGORA("CT", "Stara Zagora Province"),
    TARGOVISHTE("T", "Targovishte Province"),
    DOBRICH("TX", "Dobrich Province"),
    YAMBOL("Y", "Yambol Province"),
    HASKOVO("X", "Haskovo Province");
    //series
    private final static List<Character> VALID_SERIES_LETTERS = List.of('A', 'B', 'K', 'M', 'H', 'P', 'C', 'T', 'X');
    private final static Map<String, BulgarianLicensePlateDetails> BY_CODE = new HashMap<>();

    static {
        for (BulgarianLicensePlateDetails code : values()) {
            BY_CODE.put(code.getCode(), code);
        }
    }

    private final String code;
    private final String provinceName;

    BulgarianLicensePlateDetails(String code, String province) {
        this.code = code;
        this.provinceName = province;
    }

    public static BulgarianLicensePlateDetails getByProvinceCode(String code){
        return BY_CODE.get(code);
    }

    public static List<Character> getValidSeriesLetters() {
        return VALID_SERIES_LETTERS;
    }

    public String getCode() {
        return code;
    }

    public String getProvinceName() {
        return provinceName;
    }

}