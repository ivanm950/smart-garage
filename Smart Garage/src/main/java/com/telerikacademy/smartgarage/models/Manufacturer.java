package com.telerikacademy.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "manufacturers")
public class Manufacturer {

    // TODO: 3/31/2022 JORKATA

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "manufacturer_id")
    int id;

    @Column(name = "name")
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}