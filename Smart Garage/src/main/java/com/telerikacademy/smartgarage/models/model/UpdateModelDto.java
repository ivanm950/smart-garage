package com.telerikacademy.smartgarage.models.model;

import org.springframework.lang.Nullable;

public class UpdateModelDto {

    @Nullable
    private String name;

    public UpdateModelDto(@Nullable String name) {
        this.name = name;
    }

    public UpdateModelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }
}
