package com.telerikacademy.smartgarage.models.model;

import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

    private final ModelService modelService;
    private final BrandService brandService;

    @Autowired
    public ModelMapper(ModelService modelService, BrandService brandService) {
        this.modelService = modelService;
        this.brandService = brandService;
    }

    public Model createDtoToObject(CreateModelDto createModelDto){
        Model model = new Model();
        Brand brand = brandService.getById(createModelDto.getBrandId());
        model.setBrand(brand);
        model.setName(createModelDto.getName());

        return model;
    }

    public Model updateDtoToObject(UpdateModelDto updateModelDto,Model modelToUpdate){
        Model model1 = new Model();
        model1.setId(modelToUpdate.getId());

        if(updateModelDto.getName() != null) model1.setName(updateModelDto.getName());
        else model1.setName(modelToUpdate.getName());

        return model1;
    }
}
