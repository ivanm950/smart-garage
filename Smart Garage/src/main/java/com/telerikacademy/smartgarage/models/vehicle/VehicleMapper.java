package com.telerikacademy.smartgarage.models.vehicle;

import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;

@Component
public class VehicleMapper {

    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final ModelService modelService;

    @Autowired
    public VehicleMapper(VehicleService vehicleService, BrandService brandService, ModelService modelService) {
        this.vehicleService = vehicleService;
        this.brandService = brandService;
        this.modelService = modelService;
    }

    public Vehicle createDTOtoObject(CreateVehicleDTO createVehicleDTO, User requester) {
        Vehicle vehicle = new Vehicle();
//        Brand brand = brandService.getById(createVehicleDTO.getBrandId());
        Model model = modelService.getById(createVehicleDTO.getModelId());
        Year year = Year.of(createVehicleDTO.getYearOfCreation().getYear());

//        vehicle.setBrand(brand);
        vehicle.setModel(model);
        vehicle.setBrand(model.getBrand());
        vehicle.setVin(createVehicleDTO.getVin());
        vehicle.setLicensePlate(createVehicleDTO.getLicence_plate());
        vehicle.setOwner(requester);
        vehicle.setYearOfCreation(createVehicleDTO.getYearOfCreation());
//        vehicle.setYearOfCreation(LocalDate.from(year));

        return vehicle;
    }

    public Vehicle updateFromDto(UpdateVehicleDTO updateVehicleDTO,Vehicle vehicletoUpdate){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicletoUpdate.getId());

//        Brand brand = brandService.getById(updateVehicleDTO.getBrand_id());
//        if(updateVehicleDTO.getBrand_id() != 0) vehicle.setBrand(brand);
//        else vehicle.setBrand(vehicletoUpdate.getBrand());

        Model model = modelService.getById(updateVehicleDTO.getModel_id());
        if(updateVehicleDTO.getModel_id() != 0) vehicle.setModel( model);
        else vehicle.setModel(vehicletoUpdate.getModel());

        vehicle.setOwner(vehicletoUpdate.getOwner());

        vehicle.setYearOfCreation(vehicletoUpdate.getYearOfCreation());

        if(updateVehicleDTO.getLicence_plate() != null) vehicle.setLicensePlate(updateVehicleDTO.getLicence_plate());
        else vehicle.setLicensePlate(vehicletoUpdate.getLicensePlate());

        if(updateVehicleDTO.getVin() != null) vehicle.setVin(updateVehicleDTO.getVin());
        else vehicle.setVin(vehicletoUpdate.getVin());

        return vehicle;
    }
}
