package com.telerikacademy.smartgarage.models.service;

import org.springframework.stereotype.Component;

@Component
public class ServiceMapper {

    public Service updateFromDTO(ServiceDTO serviceDTO, Service serviceToUpdate) {
        serviceToUpdate.setPrice(serviceDTO.getPrice());
        return serviceToUpdate;
    }

    public ServiceDTO dtoFromObject(Service service) {
        ServiceDTO serviceDTO = new ServiceDTO();
        serviceDTO.setServiceType(service.getServiceType());
        serviceDTO.setPrice(service.getPrice());
        return serviceDTO;
    }

}