package com.telerikacademy.smartgarage.models.booking;

import com.telerikacademy.smartgarage.models.service.Service;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.utils.validators.constraints.Booking;

import java.time.LocalDateTime;
import java.util.List;

public class UpdateBookingDTO {

    private Vehicle vehicle;

    @Booking
    private LocalDateTime dateTime;

    private List<Service> services;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

}