package com.telerikacademy.smartgarage.models.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateUserDTO {


    @NotNull
    @Size(min = 6,max = 20,message = "Your username should be between 6 and 20 symbols.")
    private  String username;

    @NotNull
    private String email;

    @NotNull
    @Size(min = 8,max = 32,message = "Your password must be between 8 and 32 symbols")
    private String password;

    @NotNull
    @Size(min = 10,max = 10,message = "Phone numbers must be 10 symbols")
    private String phoneNumber;


    public CreateUserDTO(String username, String email, String password, String phoneNumber) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
    }

    public CreateUserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
