package com.telerikacademy.smartgarage.models.servicecategory;

import org.springframework.stereotype.Component;

@Component
public class ServiceCategoryMapper {

    public ServiceCategory createFromDTO(ServiceCategoryDTO serviceCategoryDTO){
        ServiceCategory serviceCategory = new ServiceCategory();
        serviceCategory.setName(serviceCategoryDTO.getName());
        return serviceCategory;
    }

    public ServiceCategory updateFromDTO(ServiceCategoryDTO serviceCategoryDTO, ServiceCategory serviceCategoryToUpdate){
        serviceCategoryToUpdate.setName(serviceCategoryDTO.getName());
        return serviceCategoryToUpdate;
    }

    public ServiceCategoryDTO dtoFromObject(ServiceCategory serviceCategory){
        ServiceCategoryDTO serviceCategoryDTO = new ServiceCategoryDTO();
        serviceCategoryDTO.setName(serviceCategory.getName());
        return serviceCategoryDTO;
    }

}