package com.telerikacademy.smartgarage.models.servicecategory;

import com.telerikacademy.smartgarage.models.servicetype.ServiceType;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "service_categories")
public class ServiceCategory {

    // TODO: 3/31/2022 VANKATA

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "serviceCategory", cascade = CascadeType.ALL)
    private List<ServiceType> services;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceType> getServices() {
        return services;
    }

    public void setServices(List<ServiceType> services) {
        this.services = services;
    }

}