package com.telerikacademy.smartgarage.models.servicetype;

import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceTypeMapper {

    private final ServiceCategoryService serviceCategoryService;

    @Autowired
    public ServiceTypeMapper(ServiceCategoryService serviceCategoryService) {
        this.serviceCategoryService = serviceCategoryService;
    }

    public ServiceType createFromDTO(ServiceTypeDTO serviceTypeDTO){
        ServiceType serviceType = new ServiceType();
        ServiceCategory serviceCategory = serviceCategoryService.getById(serviceTypeDTO.getCategoryId());
        serviceType.setServiceCategory(serviceCategory);
        serviceType.setName(serviceTypeDTO.getName());
        return serviceType;
    }

    public ServiceType updateFromDTO(ServiceTypeDTO serviceTypeDTO, ServiceType serviceTypeToUpdate){
        if (serviceTypeDTO.getCategoryId() != 0) {
            serviceTypeToUpdate.setServiceCategory(serviceCategoryService.getById(serviceTypeDTO.getCategoryId()));
        }
        if (serviceTypeDTO.getName() != null || !serviceTypeDTO.getName().isEmpty()){
            serviceTypeToUpdate.setName(serviceTypeDTO.getName());
        }
        return serviceTypeToUpdate;
    }

    public ServiceTypeDTO dtoFromObject(ServiceType serviceType){
        ServiceTypeDTO serviceTypeDTO = new ServiceTypeDTO();
        serviceTypeDTO.setName(serviceType.getName());
        serviceTypeDTO.setCategoryId(serviceType.getId());
        return serviceTypeDTO;
    }

}