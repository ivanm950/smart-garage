package com.telerikacademy.smartgarage.models.booking;

import com.telerikacademy.smartgarage.models.user.User;
import org.springframework.stereotype.Component;

@Component
public class BookingMapper {

    public Booking createFromDTO(CreateBookingDTO createBookingDTO, User requester) {
        Booking booking = new Booking();
        booking.setServices(createBookingDTO.getServices());
        booking.setVehicle(createBookingDTO.getVehicle());
        return booking;
    }

    public Booking updateFromDTO(UpdateBookingDTO updateBookingDTO, Booking bookingToUpdate) {
        if (updateBookingDTO.getServices() != null) bookingToUpdate.setServices(updateBookingDTO.getServices());
        if (updateBookingDTO.getVehicle() != null) bookingToUpdate.setVehicle(updateBookingDTO.getVehicle());
        if (updateBookingDTO.getDateTime() != null) bookingToUpdate.setDropOffDate(updateBookingDTO.getDateTime());
        return bookingToUpdate;
    }

    public UpdateBookingDTO dtoFromObject(Booking booking){
        UpdateBookingDTO bookingDTO = new UpdateBookingDTO();
        bookingDTO.setVehicle(booking.getVehicle());
        bookingDTO.setDateTime(booking.getDropOffDate());
        bookingDTO.setServices(booking.getServices());
        return bookingDTO;
    }

}