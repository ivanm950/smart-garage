package com.telerikacademy.smartgarage.models.brand;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateBrandDto {

    @NotNull
    @Size(min = 3,max = 12,message = "Brand name should be between 1 and 12 sybmols")
    public String name;


    public CreateBrandDto(String name) {
        this.name = name;
    }

    public CreateBrandDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
