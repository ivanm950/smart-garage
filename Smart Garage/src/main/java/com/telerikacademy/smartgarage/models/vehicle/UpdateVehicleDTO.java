package com.telerikacademy.smartgarage.models.vehicle;

import com.telerikacademy.smartgarage.utils.validators.constraints.LicensePlateNumber;
import com.telerikacademy.smartgarage.utils.validators.constraints.Year;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class UpdateVehicleDTO {

    @Nullable
    private int brand_id;

    @Nullable
    private int model_id;

    @Nullable
    @LicensePlateNumber
    private String licence_plate;

    @Nullable
    private String vin;

//    @Nullable
//    @Year
//    private LocalDate year_Of_Creation;


    public UpdateVehicleDTO(int brand_id, int model_id, @Nullable String licence_plate, @Nullable String vin) {
        this.brand_id = brand_id;
        this.model_id = model_id;
        this.licence_plate = licence_plate;
        this.vin = vin;
    }

    public UpdateVehicleDTO() {
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getModel_id() {
        return model_id;
    }

    public void setModel_id(int model_id) {
        this.model_id = model_id;
    }

    public String getLicence_plate() {
        return licence_plate;
    }

    public void setLicence_plate( String licence_plate) {
        this.licence_plate = licence_plate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin( String vin) {
        this.vin = vin;
    }


}
