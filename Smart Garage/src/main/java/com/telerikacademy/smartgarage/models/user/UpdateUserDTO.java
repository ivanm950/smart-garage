package com.telerikacademy.smartgarage.models.user;

import org.springframework.lang.Nullable;

import javax.servlet.http.PushBuilder;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDTO {

    @Nullable
    @Size(min = 6,max = 20,message = "Your username should be between 6 and 20 symbols.")
    public String username;

    @Nullable
    @Email
    private String email;

    @Nullable
    @Size(min = 8,max = 32,message = "Your password must be between 8 and 32 symbols")
    private String password;

    @Nullable
    @Size(min = 10,max = 10,message = "Phone numbers must be 10 symbols")
    private String phoneNumber;

    @NotNull
    private int roleId;


    public UpdateUserDTO(@Nullable String username, @Nullable String email, @Nullable String password, @Nullable String phoneNumber, int roleId) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.roleId = roleId;
    }

    public UpdateUserDTO() {
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername( String username) {
        if(username.isEmpty()) username = null;
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String email) {
        if(email.isEmpty()) email = null;
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password) {
        if (password.isEmpty()) password = null;
        this.password = password;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber( String phoneNumber) {
        if(phoneNumber.isEmpty()) phoneNumber = null;
        this.phoneNumber = phoneNumber;
    }
}
