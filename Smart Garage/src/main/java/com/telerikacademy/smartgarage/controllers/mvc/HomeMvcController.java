package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final VehicleService vehicleService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, UserService userService, VehicleService vehicleService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.vehicleService = vehicleService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> popualteVehicles(){
        return vehicleService.getAll();
    }

    @GetMapping
    public String showHomepage(Model model, HttpSession httpSession) {
        try{
            String username = (String) httpSession.getAttribute("currentUser");
            User requester = userService.getByUsername(username);
            model.addAttribute("requester",requester);
        } catch (EntityNotFoundException e){
            return "index";
        }
        return "index";
    }

    @GetMapping("/login")
    public String showLoginPage(Model model,HttpSession httpSession){
        return "login";
    }

}