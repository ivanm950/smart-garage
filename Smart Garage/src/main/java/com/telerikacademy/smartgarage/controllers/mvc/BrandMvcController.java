package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.brand.BrandMapper;
import com.telerikacademy.smartgarage.models.brand.CreateBrandDto;
import com.telerikacademy.smartgarage.models.brand.UpdateBrandDto;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/brands")
public class BrandMvcController {

    private final BrandService brandService;
    private final AuthenticationHelper authenticationHelper;
    private final BrandMapper brandMapper;

    @Autowired
    public BrandMvcController(BrandService brandService, AuthenticationHelper authenticationHelper, BrandMapper brandMapper) {
        this.brandService = brandService;
        this.authenticationHelper = authenticationHelper;
        this.brandMapper = brandMapper;
    }

    @GetMapping
    public String getAll(HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            List<Brand> brandList = brandService.getAll();

            model.addAttribute("allbrands", brandList);
            model.addAttribute("requester", requester);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "brands";
    }

    @GetMapping("{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            User requseter = authenticationHelper.tryGetUser(httpSession);
            Brand brand = brandService.getById(id);
            model.addAttribute("requester", requseter);
            model.addAttribute("brand", brand);
            return "brand";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/create")
    public String showCreatePage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized to create brands");
            }
            model.addAttribute("createbrand", new CreateBrandDto());
            return "create-brand";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/create")
    public String handleCraetePage(@Valid @ModelAttribute("createbrand") CreateBrandDto createBrandDto,
                                   BindingResult bindingResult,
                                   HttpSession httpSession,
                                   Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized to create brands");
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        model.addAttribute("createVehicle", createBrandDto);

        if (bindingResult.hasErrors()) {
            return "create-brand";
        }

        try {
            Brand brand = brandMapper.createDtoToObject(createBrandDto);
            brandService.create(brand);
            return "redirect:/brands/" + brand.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "brand_errors", e.getMessage());
            return "create-brand";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditPage(@PathVariable int id, Model model, HttpSession httpSession) {

        User requester;
        Brand brandToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            brandToUpdate = brandService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("requester", requester);
            model.addAttribute("brandToUpdate", brandToUpdate);
            model.addAttribute("updatebrand", new UpdateBrandDto());
            return "update-brand";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("updatebrand") UpdateBrandDto updateBrandDto,
                                 BindingResult bindingResult,
                                 HttpSession httpSession,
                                 Model model){
        User requester;
        Brand brandToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            brandToUpdate = brandService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("brandToUpdate", brandToUpdate);
            if (bindingResult.hasErrors()) {
                return "update-brand";
            }
            Brand brand = brandMapper.updateBrandFromDto(updateBrandDto, brandToUpdate);
            brandService.update(brand);
            return "redirect:/brands/" + brand.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "brand_error", e.getMessage());
            return "update-brand";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteBrand(@PathVariable int id,HttpSession httpSession,Model model){
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Brand vehicleToDelete = brandService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            brandService.delete(vehicleToDelete);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }
}
