package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.UpdateUserDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserMapper;
import com.telerikacademy.smartgarage.services.contracts.UserRoleService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.utils.Utility;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final UserMapper userMapper;
    private final JavaMailSender javaMailSender;

    @Autowired
    public UsersMvcController(AuthenticationHelper authenticationHelper, UserService userService, UserRoleService userRoleService, UserMapper userMapper, JavaMailSender javaMailSender) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.userMapper = userMapper;
        this.javaMailSender = javaMailSender;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String getAll(HttpSession httpSession, Model model) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            model.addAttribute("requester", requester);
            return "users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }
//    @GetMapping("/forgot_password")
//    public String showForgotPasswordForm() {
//        return "forgot_password_form";
//    }
//
//    @PostMapping("/forgot_password")
//    public String processForgotPassword(HttpServletRequest request,Model model) {
//        String email = request.getParameter("email");
//        String token = RandomString.make(30);
//
//        try {
//            userService.updateResetPasswordToken(token, email);
//            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
//            sendEmail(email, resetPasswordLink);
//            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");
//
//        } catch (EntityNotFoundException ex) {
//            model.addAttribute("error", ex.getMessage());
//        } catch (UnsupportedEncodingException | MessagingException e) {
//            model.addAttribute("error", "Error while sending email");
//        }
//
//        return "forgot_password_form";
//    }
//    public void sendEmail(String recipientEmail, String link)
//            throws MessagingException, UnsupportedEncodingException {
//        MimeMessage message = javaMailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message);
//
//        helper.setFrom("gstukaniov@62.techteamsupportandinfo.com", "Tech Support");
//        helper.setTo(recipientEmail);
//
//        String subject = "Here's the link to reset your password";
//
//        String content = "<p>Hello,</p>"
//                + "<p>You have requested to reset your password.</p>"
//                + "<p>Click the link below to change your password:</p>"
//                + "<p><a href=\"" + link + "\">Change my password</a></p>"
//                + "<br>"
//                + "<p>Ignore this email if you do remember your password, "
//                + "or you have not made the request.</p>";
//
//        helper.setSubject(subject);
//
//        helper.setText(content, true);
//
//        javaMailSender.send(message);
//    }
//
//    @GetMapping("/reset_password")
//    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
//        User user = userService.getByResetPasswordToken(token);
//        model.addAttribute("token", token);
//
//        if (user == null) {
//            model.addAttribute("message", "Invalid Token");
//            return "message";
//        }
//
//        return "reset_password_form";
//    }
//
//    @PostMapping("/reset_password")
//    public String processResetPassword(HttpServletRequest request,Model model) {
//        String token = request.getParameter("token");
//        String password = request.getParameter("password");
//
//        User user = userService.getByResetPasswordToken(token);
//        model.addAttribute("title", "Reset your password");
//
//        if (user == null) {
//            model.addAttribute("message", "Invalid Token");
//            return "message";
//        } else {
//            userService.updatePassword(user, password);
//
//            model.addAttribute("message", "You have successfully changed your password.");
//        }
//
//        return "reset_password_form";
//    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            User userToGet = userService.getById(id);
            if (!requester.isEmployee() && requester.getId() != userToGet.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("requester", requester);
            model.addAttribute("user", userToGet);
            return "user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("{id}/edit")
    public String showEditpage(@PathVariable int id, HttpSession httpSession, Model model) {
        User requester;
        User userToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            userToUpdate = userService.getById(id);
            if (!requester.isEmployee() || requester.getId() != userToUpdate.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("requester", requester);
            model.addAttribute("userToUpdate", userToUpdate);
            model.addAttribute("editprofile", new UpdateUserDTO());
            return "user-edit";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("editprofile") UpdateUserDTO updateUserDTO,
                                 BindingResult bindingResult,
                                 HttpSession httpSession,
                                 Model model) {
        User requester;
        User userToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            userToUpdate = userService.getById(id);
            if (!requester.isEmployee() || requester.getId() != userToUpdate.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("userToUpdate", userToUpdate);
            if (bindingResult.hasErrors()) {
                return "user-edit";
            }
            User user = userMapper.updateDtoToObject(updateUserDTO, userToUpdate);
            userService.update(requester, user);
            return "redirect:/users/" + user.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_err", "Email is already taken");
            return "user-edit";
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id,
                             Model model,
                             HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            User userToDelete = userService.getById(id);
            if (!requester.isEmployee() || requester.getId() != userToDelete.getId()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            userService.delete(userToDelete);
            if (!requester.isEmployee()) httpSession.removeAttribute("currentUser");
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }



}