package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.service.ServiceDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceType;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeDTO;
import com.telerikacademy.smartgarage.models.servicetype.ServiceTypeMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import com.telerikacademy.smartgarage.services.contracts.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/services")
public class ServiceTypeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceTypeService serviceTypeService;
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceTypeMapper serviceTypeMapper;

    @Autowired
    public ServiceTypeMvcController(AuthenticationHelper authenticationHelper,
                                    ServiceTypeService serviceTypeService,
                                    ServiceCategoryService serviceCategoryService,
                                    ServiceTypeMapper serviceTypeMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceTypeService = serviceTypeService;
        this.serviceCategoryService = serviceCategoryService;
        this.serviceTypeMapper = serviceTypeMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAll(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            List<ServiceCategory> categories = serviceCategoryService.getAll();
            model.addAttribute("requester", requester);
            model.addAttribute("categories", categories);
            return "services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/create")
    public String showCreatePage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("serviceType", new ServiceDTO());
            return "service-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("serviceType") ServiceTypeDTO serviceTypeDTO,
                         BindingResult bindingResult,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                return "service-manager";
            }
            ServiceType serviceType = serviceTypeMapper.createFromDTO(serviceTypeDTO);
            serviceTypeService.create(serviceType, requester);
            return "redirect:/services/" + serviceType.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditPage(@PathVariable int id,
                               Model model,
                               HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceType serviceTypeToUpdate = serviceTypeService.getById(id);
            ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.dtoFromObject(serviceTypeToUpdate);
            model.addAttribute("serviceType", serviceTypeDTO);
            return "service-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("serviceType") ServiceTypeDTO serviceTypeDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                return "service-manager";
            }
            ServiceType serviceTypeToUpdate = serviceTypeService.getById(id);
            ServiceType updatedServiceType = serviceTypeMapper.updateFromDTO(serviceTypeDTO, serviceTypeToUpdate);
            serviceTypeService.update(updatedServiceType, requester);
            return "";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            ServiceType serviceTypeToDelete = serviceTypeService.getById(id);
            serviceTypeService.delete(serviceTypeToDelete, requester);
            return "redirect:/services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

}