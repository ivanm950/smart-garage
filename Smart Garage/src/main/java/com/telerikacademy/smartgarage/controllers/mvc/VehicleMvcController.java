package com.telerikacademy.smartgarage.controllers.mvc;


import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.vehicle.CreateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.VehicleMapper;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleMapper vehicleMapper;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService, BrandService brandService, ModelService modelService, AuthenticationHelper authenticationHelper, VehicleMapper vehicleMapper) {
        this.vehicleService = vehicleService;
        this.brandService = brandService;
        this.modelService = modelService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleMapper = vehicleMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("allbrands")
    public List<Brand> popualteBrands() {
        return brandService.getAll();
    }

    @ModelAttribute("allmodels")
    public List<com.telerikacademy.smartgarage.models.model.Model> populateModels() {
        return modelService.getAll();
    }


    @GetMapping
    public String getAll(HttpSession httpSession, Model model) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            List<Vehicle> vehicleList;
            if (requester.isEmployee()) vehicleList = vehicleService.getAll();
            else vehicleList = vehicleService.getAllByFieldMatches("owner.id", requester.getId());
            model.addAttribute("allvehicles", vehicleList);
            model.addAttribute("requester", requester);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "vehicles";
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {
        Vehicle vehicle;
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            vehicle = vehicleService.getById(id);

            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicle))) {
                return "redirect:/";
            }
            model.addAttribute("requester", requester);
            model.addAttribute("vehicle", vehicle);

            return "vehicle";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/create")
    public String showVehicleCreatePage(Model model) {
        model.addAttribute("createVehicle", new CreateVehicleDTO());
        return "create-vehicle";
    }

    @PostMapping("/create")
    public String handleVehicleCreatePage(Model model,
                                          @Valid @ModelAttribute("createVehicle") CreateVehicleDTO createVehicleDTO,
                                          BindingResult bindingResult,
                                          HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        model.addAttribute("createVehicle", createVehicleDTO);
        if (bindingResult.hasErrors()) {
            return "create-vehicle";
        }

        try {
            Vehicle vehicle = vehicleMapper.createDTOtoObject(createVehicleDTO, requester);
            vehicleService.create(vehicle);
            return "redirect:/vehicles/" + vehicle.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vin_err", "A vehicle with vin symbols already exists");
            return "create-vehicle";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditPage(@PathVariable int id, Model model, HttpSession httpSession) {
        User requester;
        Vehicle vehicleToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            vehicleToUpdate = vehicleService.getById(id);
            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicleToUpdate))) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("requester", requester);
            model.addAttribute("vehicleToUpdate", vehicleToUpdate);
            model.addAttribute("updatevehicle", new UpdateVehicleDTO());
            return "update-vehicle";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @PostMapping("/{id}/edit")
    public String handleEditPage(@PathVariable int id,
                                 @Valid @ModelAttribute("updatevehicle") UpdateVehicleDTO updateVehicleDTO,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession httpSession) {

        User requester;
        Vehicle vehicleToUpdate;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            vehicleToUpdate = vehicleService.getById(id);
            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicleToUpdate))) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            model.addAttribute("vehicleToUpdate", vehicleToUpdate);
            if (bindingResult.hasErrors()) {
                return "update-vehicle";
            }
            Vehicle vehicle = vehicleMapper.updateFromDto(updateVehicleDTO, vehicleToUpdate);
            vehicleService.update(vehicle);
            return "redirect:/vehicles/" + vehicle.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vin_error", "Vin is already taken");
            return "update-vehicle";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteVehicle(@PathVariable int id, Model model, HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Vehicle vehicleToDelete = vehicleService.getById(id);
            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicleToDelete))) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            vehicleService.delete(vehicleToDelete);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }
}


