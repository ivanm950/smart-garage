package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.user.UpdateUserDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.CreateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.UpdateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.models.vehicle.VehicleMapper;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final AuthenticationHelper authenticationHelper;
    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;

    public VehicleController(AuthenticationHelper authenticationHelper, VehicleService vehicleService, VehicleMapper vehicleMapper) {
        this.authenticationHelper = authenticationHelper;
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized");
            }
            return vehicleService.getAll();
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicleToget = vehicleService.getById(id);
            if (!(requester.isEmployee() || requester.getVehicles().contains(vehicleToget))) {
                throw new UnauthorisedOperationException("You are neither employee nor owner ");
            }
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Vehicle createVehicle(@Valid @RequestBody CreateVehicleDTO createVehicleDTO,
                                 @RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicle = vehicleMapper.createDTOtoObject(createVehicleDTO,requester);
            vehicleService.create(vehicle);
            return vehicle;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Vehicle update(@PathVariable int id,
                       @Valid @RequestBody UpdateVehicleDTO updateVehicleDTO,
                       @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Vehicle vehicleToUpdate = vehicleService.getById(id);
            Vehicle updateVehicle = vehicleMapper.updateFromDto(updateVehicleDTO, vehicleToUpdate);
            vehicleService.update(updateVehicle);
            return updateVehicle;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Vehicle delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requester = authenticationHelper.tryGetUser(headers);
            Vehicle vehicleToDelete = vehicleService.getById(id);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to delete user");
            }
            vehicleService.delete(vehicleToDelete);
            return vehicleToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


}
