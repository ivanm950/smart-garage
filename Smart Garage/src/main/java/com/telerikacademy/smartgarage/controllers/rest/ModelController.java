package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.brand.CreateBrandDto;
import com.telerikacademy.smartgarage.models.brand.UpdateBrandDto;
import com.telerikacademy.smartgarage.models.model.CreateModelDto;
import com.telerikacademy.smartgarage.models.model.Model;
import com.telerikacademy.smartgarage.models.model.ModelMapper;
import com.telerikacademy.smartgarage.models.model.UpdateModelDto;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/models")
public class ModelController {

    private final AuthenticationHelper authenticationHelper;
    private final ModelService modelService;
    private final ModelMapper modelMapper;

    public ModelController(AuthenticationHelper authenticationHelper, ModelService modelService, ModelMapper modelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.modelService = modelService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Model> getll(@RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized");
            }
            return modelService.getAll();
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Model getById(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Model modelToUpdate = modelService.getById(id);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to get this resource");
            }
            return modelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Model createModel(@Valid @RequestBody CreateModelDto createModelDto,
                             @RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Model model = modelMapper.createDtoToObject(createModelDto);
            modelService.create(model);
            return model;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Model update(@PathVariable int id,
                        @Valid @RequestBody UpdateModelDto updateModelDto,
                        @RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Model modelToUpdate = modelService.getById(id);
            Model updateModel = modelMapper.updateDtoToObject(updateModelDto, modelToUpdate);
            modelService.update(updateModel);
            return updateModel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Model delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requester = authenticationHelper.tryGetUser(headers);
            Model modelToDelete = modelService.getById(id);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to delete user");
            }
            modelService.delete(modelToDelete);
            return modelToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


}
