package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.models.user.CreateUserDTO;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.user.UserMapper;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public RegistrationController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping
    public String getRegisterPage(Model model){
        model.addAttribute("register",new CreateUserDTO());
        return "registration";
    }

    @PostMapping
    public String handleRegisterPage(Model model,
                                     @Valid @ModelAttribute("register") CreateUserDTO createUserDTO,
                                     BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "registration";
        }

        try {
            User user = userMapper.createDtoToObject(createUserDTO);
            userService.create(user);
            return "redirect:/";
        }catch (DuplicateEntityException e){
            if (e.getMessage().contains("username and email")) {
                bindingResult.rejectValue("username", "username-err", "Username already exists");
                bindingResult.rejectValue("email", "email-err", "User with this email already exists");
                return "registration";
            } else if (e.getMessage().contains("username")) {
                bindingResult.rejectValue("username", "username-err", "Username already exists");
                return "registration";
            } else {
                bindingResult.rejectValue("email", "email-err", "User with this email already exists");
                return "registration";
            }
        }
    }
}
