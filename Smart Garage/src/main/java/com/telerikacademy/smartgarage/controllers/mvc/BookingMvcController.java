package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.booking.Booking;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.booking.BookingMapper;
import com.telerikacademy.smartgarage.models.booking.CreateBookingDTO;
import com.telerikacademy.smartgarage.models.booking.UpdateBookingDTO;
import com.telerikacademy.smartgarage.services.contracts.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/bookings")
public class BookingMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final BookingService bookingService;
    private final BookingMapper bookingMapper;

    @Autowired
    public BookingMvcController(BookingService bookingService, AuthenticationHelper authenticationHelper, BookingMapper bookingMapper) {
        this.bookingService = bookingService;
        this.authenticationHelper = authenticationHelper;
        this.bookingMapper = bookingMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllBookings(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            List<Booking> bookings = new ArrayList<>();
            if (requester.isEmployee()) bookings = bookingService.getAll();
            else bookings = bookingService.getAllByFieldMatches("vehicle.owner.id", requester.getId());
            model.addAttribute("bookings", bookings);
            model.addAttribute("dateFormat","d MMMM yyyy");
            model.addAttribute("yearFormat","yyyy");
            return "bookings";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String showBooking(@PathVariable int id,
                              Model model,
                              HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Booking booking = bookingService.getById(id);
            if (!requester.isEmployee() && requester.getId() != booking.getVehicle().getOwner().getId()) {
                model.addAttribute("error", "You're not authorised to access this resource");
                return "error-page";
            }
            model.addAttribute("booking", booking);
            return "booking";
        } catch (AuthenticationFailureException e) {
            return "redirect:auth/login";
        }
    }

    @GetMapping("/create")
    public String showCreatePage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("booking", new CreateBookingDTO());
            return "booking-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("booking") CreateBookingDTO createBookingDTO,
                         BindingResult bindingResult,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                return "booking-manager";
            }
            Booking booking = bookingMapper.createFromDTO(createBookingDTO, requester);
            bookingService.create(booking);
            return "redirect:/bookings/" + booking.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditPage(@PathVariable int id,
                               Model model,
                               HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            Booking bookingToUpdate = bookingService.getById(id);
            if (!requester.isEmployee() && requester.getId() != bookingToUpdate.getVehicle().getOwner().getId()) {
                throw new UnsupportedOperationException("You're not authorised to modify this resource");
            }
            UpdateBookingDTO updateBookingDTO = bookingMapper.dtoFromObject(bookingToUpdate);
            model.addAttribute("booking", updateBookingDTO);
            return "booking-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("booking") UpdateBookingDTO updateBookingDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                model.addAttribute("booking", updateBookingDTO);
                return "booking-manager";
            }
            Booking bookingToUpdate = bookingService.getById(id);
            Booking updatedBooking = bookingMapper.updateFromDTO(updateBookingDTO, bookingToUpdate);
            bookingService.update(updatedBooking);
            return "redirect:/bookings/" + id;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            Booking bookingToDelete = bookingService.getById(id);
            bookingService.delete(bookingToDelete, requester);
            return "redirect:/bookings" + id;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

}