package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.model.CreateModelDto;
import com.telerikacademy.smartgarage.models.model.ModelMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/models")
public class ModelMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ModelService modelService;
    private final ModelMapper modelMapper;

    @Autowired
    public ModelMvcController(AuthenticationHelper authenticationHelper, ModelService modelService, ModelMapper modelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.modelService = modelService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public String getAll(HttpSession httpSession, Model model){
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to get this resource");
            }
        }catch (AuthenticationFailureException e){
            return "redirect:/auth/login";
        }catch (UnauthorisedOperationException e){
            model.addAttribute("error",e.getMessage());
            return "errors";
        }
        List<com.telerikacademy.smartgarage.models.model.Model> modelList = modelService.getAll();
        model.addAttribute("allmodels",modelList);
        return "models";
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, HttpSession httpSession, Model model) {

        com.telerikacademy.smartgarage.models.model.Model model1;
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model1 = modelService.getById(id);

            if (!requester.isEmployee()) {
                return "redirect:/";
            }
            model.addAttribute("requester", requester);
            model.addAttribute("model", model1);

            return "model";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }

    @GetMapping("/create")
    public String showVehicleCreatePage(Model model) {
        model.addAttribute("createModel", new CreateModelDto());
        return "create-model";
    }

    @PostMapping("/create")
    public String handleVehicleCreatePage(Model model,
                                          @Valid @ModelAttribute("createModel") CreateModelDto createModelDto,
                                          BindingResult bindingResult,
                                          HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requester", requester);
        model.addAttribute("createModel", createModelDto);

        if (bindingResult.hasErrors()) {
            return "create-model";
        }

        try {
            com.telerikacademy.smartgarage.models.model.Model model1 = modelMapper.createDtoToObject(createModelDto);
            modelService.create(model1);
            return "redirect:/models/" + model1.getId();
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "model_err", "A model already exists");
            return "create-model";
        }
    }
    @GetMapping("{id}/delete")
    public String deleteModel(@PathVariable int id, Model model, HttpSession httpSession) {
        User requester;
        try {
            requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);

            com.telerikacademy.smartgarage.models.model.Model modelToDelete = modelService.getById(id);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You're not authorised to access to the requested resource");
            }
            modelService.delete(modelToDelete);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors";
        }
    }


}
