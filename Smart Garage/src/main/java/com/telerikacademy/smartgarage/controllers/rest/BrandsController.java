package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.brand.Brand;
import com.telerikacademy.smartgarage.models.brand.BrandMapper;
import com.telerikacademy.smartgarage.models.brand.CreateBrandDto;
import com.telerikacademy.smartgarage.models.brand.UpdateBrandDto;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.models.vehicle.CreateVehicleDTO;
import com.telerikacademy.smartgarage.models.vehicle.Vehicle;
import com.telerikacademy.smartgarage.services.contracts.BrandService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/brands")
public class BrandsController {

    private final AuthenticationHelper authenticationHelper;
    private final BrandService brandService;
    private final BrandMapper brandMapper;

    public BrandsController(AuthenticationHelper authenticationHelper, BrandService brandService, BrandMapper brandMapper) {
        this.authenticationHelper = authenticationHelper;
        this.brandService = brandService;
        this.brandMapper = brandMapper;
    }

    @GetMapping
    public List<Brand> getAll(@RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            if (!requester.isEmployee()) {
                throw new UnauthorisedOperationException("You are not authorized");
            }
            return brandService.getAll();
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Brand getById(@PathVariable int id, @RequestHeader HttpHeaders httpHeaders) {
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Brand brandToupdate = brandService.getById(id);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to get this resource");
            }
            return brandService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Brand createBrand(@Valid @RequestBody CreateBrandDto createBrandDto,
                                 @RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Brand brand = brandMapper.createDtoToObject(createBrandDto);
            brandService.create(brand);
            return brand;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brand update(@PathVariable int id,
                        @Valid @RequestBody UpdateBrandDto updateBrandDto,
                        @RequestHeader HttpHeaders httpHeaders){
        try {
            User requester = authenticationHelper.tryGetUser(httpHeaders);
            Brand brandToUpdate = brandService.getById(id);
            Brand updateBrand = brandMapper.updateBrandFromDto(updateBrandDto, brandToUpdate);
            brandService.update(updateBrand);
            return updateBrand;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public Brand delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requester = authenticationHelper.tryGetUser(headers);
            Brand brandToDelete = brandService.getById(id);
            if(!requester.isEmployee()){
                throw new UnauthorisedOperationException("You are not authorized to delete user");
            }
            brandService.delete(brandToDelete);
            return brandToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }




}
