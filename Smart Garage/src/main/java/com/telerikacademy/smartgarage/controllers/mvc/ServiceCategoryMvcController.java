package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorisedOperationException;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategory;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryDTO;
import com.telerikacademy.smartgarage.models.servicecategory.ServiceCategoryMapper;
import com.telerikacademy.smartgarage.models.user.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/categories")
public class ServiceCategoryMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceCategoryMapper serviceCategoryMapper;

    @Autowired
    public ServiceCategoryMvcController(AuthenticationHelper authenticationHelper, ServiceCategoryService serviceCategoryService, ServiceCategoryMapper serviceCategoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.serviceCategoryService = serviceCategoryService;
        this.serviceCategoryMapper = serviceCategoryMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllPage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            return "index";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/create")
    public String showCreatePage(Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            model.addAttribute("serviceCategory", new ServiceCategoryDTO());
            return "category-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("serviceCategory") ServiceCategoryDTO serviceCategoryDTO,
                         BindingResult bindingResult,
                         Model model,
                         HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                return "category-manager";
            }
            ServiceCategory serviceCategory = serviceCategoryMapper.createFromDTO(serviceCategoryDTO);
            serviceCategoryService.create(serviceCategory, requester);
            return "redirect:/categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditPage(@PathVariable int id,
                               Model model,
                               HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceCategory serviceCategoryToUpdate = serviceCategoryService.getById(id);
            ServiceCategoryDTO serviceCategoryDTO = serviceCategoryMapper.dtoFromObject(serviceCategoryToUpdate);
            model.addAttribute("serviceCategory", serviceCategoryDTO);
            return "category-manager";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @PostMapping("/{id}/edit")
    public String edit(@PathVariable int id,
                       @Valid @ModelAttribute("serviceCategory") ServiceCategoryDTO serviceCategoryDTO,
                       BindingResult bindingResult,
                       Model model,
                       HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            if (bindingResult.hasErrors()) {
                return "category-manager";
            }
            ServiceCategory serviceCategoryToUpdate = serviceCategoryService.getById(id);
            ServiceCategory updatedCategory = serviceCategoryMapper.updateFromDTO(serviceCategoryDTO, serviceCategoryToUpdate);
            serviceCategoryService.create(updatedCategory, requester);
            return "redirect:/categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User requester = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("requester", requester);
            ServiceCategory serviceCategoryToDelete = serviceCategoryService.getById(id);
            serviceCategoryService.delete(serviceCategoryToDelete, requester);
            return "redirect:/categories";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | UnauthorisedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

}