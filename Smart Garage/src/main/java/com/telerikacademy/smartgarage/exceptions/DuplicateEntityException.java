package com.telerikacademy.smartgarage.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public DuplicateEntityException(String type, String attributes, String value1, String value2) {
        super(String.format("%s with %s %s %s already exists.", type, attributes, value1,value2));
    }
    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String message) {
        super(message);
    }
}