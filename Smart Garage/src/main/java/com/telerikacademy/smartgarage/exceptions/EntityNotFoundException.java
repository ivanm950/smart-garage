package com.telerikacademy.smartgarage.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String entityType, int id) {
        this(entityType, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String entityType, String attribute, String value) {
        super(String.format("%s with %s %s not found", entityType, attribute, value));
    }

}