-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for smart-garage
CREATE DATABASE IF NOT EXISTS `smart-garage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smart-garage`;

-- Dumping structure for table smart-garage.bookings
CREATE TABLE IF NOT EXISTS `bookings` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.bookings: ~0 rows (approximately)
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;

-- Dumping structure for table smart-garage.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.brands: ~3 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT IGNORE INTO `brands` (`brand_id`, `name`) VALUES
	(1, 'BMW'),
	(2, 'Mercedes'),
	(3, 'Land Rover');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table smart-garage.models
CREATE TABLE IF NOT EXISTS `models` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.models: ~3 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT IGNORE INTO `models` (`model_id`, `name`) VALUES
	(1, 'X3'),
	(2, 'E class'),
	(3, 'Range Rover');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table smart-garage.password_reset_token
CREATE TABLE IF NOT EXISTS `password_reset_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `password_reset_token_users_user_id_fk` (`user_id`),
  CONSTRAINT `password_reset_token_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.password_reset_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_reset_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_token` ENABLE KEYS */;

-- Dumping structure for table smart-garage.services
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`),
  KEY `services_service_types_service_id_fk` (`service_type_id`),
  KEY `services_users_user_id_fk` (`customer_id`),
  KEY `services_vehicles_vehicle_id_fk` (`vehicle_id`),
  CONSTRAINT `services_service_types_service_id_fk` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `services_users_user_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `services_vehicles_vehicle_id_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.services: ~0 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table smart-garage.service_categories
CREATE TABLE IF NOT EXISTS `service_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.service_categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `service_categories` DISABLE KEYS */;
INSERT IGNORE INTO `service_categories` (`category_id`, `name`) VALUES
	(1, 'General Maintenance'),
	(2, 'Tire Services'),
	(3, 'Transmission Services'),
	(4, 'Engine Services'),
	(5, 'General Services');
/*!40000 ALTER TABLE `service_categories` ENABLE KEYS */;

-- Dumping structure for table smart-garage.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `service_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`service_type_id`),
  KEY `service_types_service_categories_id_fk` (`category_id`),
  CONSTRAINT `service_types_service_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `service_categories` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.service_types: ~48 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT IGNORE INTO `service_types` (`service_type_id`, `name`, `price`, `category_id`) VALUES
	(1, 'Oil Changes', 1, 1),
	(2, 'Tune Ups', 1, 1),
	(3, 'Filter Replacements', 1, 1),
	(4, 'Safety & Emissions Inspections', 1, 1),
	(5, 'Windshield Wiper Blades', 1, 1),
	(6, 'Fluid Services', 1, 1),
	(7, 'Trip Inspections', 1, 1),
	(8, 'Maintenance Inspections', 1, 1),
	(9, 'Check Engine Light Diagnostics', 1, 1),
	(10, 'Tire Rotation', 1, 2),
	(11, 'Tire Mounting', 1, 2),
	(12, 'Tire Balancing', 1, 2),
	(13, 'Flat Tire Repair', 1, 2),
	(14, 'Tire Replacement', 1, 2),
	(15, 'Wheel Alignment', 1, 2),
	(16, 'Wheel Repair & Replacement', 1, 2),
	(17, 'Transmission Service & Repair', 1, 3),
	(18, 'New & Rebuilt Transmissions', 1, 3),
	(19, 'Driveline Maintenance & Repair', 1, 3),
	(20, 'Axle Replacement', 1, 3),
	(21, 'Differential Service & Repair', 1, 3),
	(22, 'Clutch Repair & Replacement', 1, 3),
	(23, 'Engine Repair', 1, 4),
	(24, 'Engine Replacement', 1, 4),
	(25, 'Engine Performance Check', 1, 4),
	(26, 'Belt & Hose Replacement', 1, 4),
	(27, 'Drivability Diagnostics & Repair', 1, 4),
	(28, 'Fuel Injection Service & Repair', 1, 4),
	(29, 'Fuel System Maintenance & Repair', 1, 4),
	(30, 'Ignition System Maintenance & Repair', 1, 4),
	(31, 'Electronic Troubleshooting', 1, 5),
	(32, 'Computer Diagnostics', 1, 5),
	(33, 'Air Conditioning Service & Repair', 1, 5),
	(34, 'Electrical System Diagnosis & Repair', 1, 5),
	(35, 'Brake Repair', 1, 5),
	(36, 'Suspension & Steering Repair', 1, 5),
	(37, 'Shocks & Struts', 1, 5),
	(38, 'Cooling System Service & Repair\n', 1, 5),
	(39, 'Exhaust Systems & Mufflers', 1, 5),
	(40, 'Custom Exhaust', 1, 5),
	(41, 'Pre-Purchase Inspections', 1, 5),
	(42, 'Brake & Lamp Inspections', 1, 5),
	(43, 'Fleet Services', 1, 5),
	(44, 'Hybrid Services', 1, 5),
	(45, 'Chassis & Suspension', 1, 5),
	(46, 'Power Steering Repair', 1, 5),
	(47, 'Body & Trim Repairs (Mirrors, Door Handles, Locks)', 1, 5),
	(48, 'Power Accessory Repair', 1, 5);
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping structure for table smart-garage.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `is_employee` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_phone_number_uindex` (`phone_number`),
  KEY `users_user_roles_role_id_fk` (`role_id`),
  CONSTRAINT `users_user_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`user_id`, `username`, `email`, `password`, `phone_number`, `is_employee`, `role_id`) VALUES
	(1, 'gstukanyov', 'gstukaniov@gmail.com', 'gogo1234', '0894611298', 0, 2),
	(2, 'vankata', 'ivanm950@yahoo.com', 'vankata123', '0894615481', 1, 2),
	(3, 'sergiokonsta', 'batsergi@yahoo.com', 'sergio1234', '0864611299', 0, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table smart-garage.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.user_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT IGNORE INTO `user_roles` (`role_id`, `name`) VALUES
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping structure for table smart-garage.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `license_plate` varchar(8) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `year_of_creation` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `model_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`vehicle_id`),
  KEY `vehicles_brands_brand_id_fk` (`brand_id`),
  KEY `vehicles_models_model_id_fk` (`model_id`),
  KEY `vehicles_users_user_id_fk` (`owner_id`),
  CONSTRAINT `vehicles_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `vehicles_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`),
  CONSTRAINT `vehicles_users_user_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table smart-garage.vehicles: ~7 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT IGNORE INTO `vehicles` (`vehicle_id`, `owner_id`, `license_plate`, `vin`, `year_of_creation`, `model_id`, `brand_id`) VALUES
	(5, 1, 'CB8765XT', 'JH4NA1150RT000268', '2022-04-11 00:00:00', 1, 1),
	(6, 2, 'PK8543BR', '1FTRW08L13KB17454', '2008-04-17 00:00:00', 2, 2),
	(7, 3, 'BR1362XT', '1FMZU34E2XCB34385', '2011-06-23 00:00:00', 3, 3),
	(8, 1, 'CB9832CB', '4S3BK6354S6355265', '2010-01-13 00:00:00', 3, 3),
	(9, 1, 'CB1234CB', 'TRUWT28N651011265', '2021-03-25 00:00:00', 1, 1),
	(10, 1, 'CB9832CB', '1YVHP84DX55M13025', '2022-04-08 00:00:00', 1, 1),
	(11, 1, 'CB9832CB', 'SALAE25425A328685', '2026-03-12 00:00:00', 3, 3);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
