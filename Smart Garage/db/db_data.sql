-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `smart-garage`;

-- Dumping data for table smart-garage.bookings: ~1 rows (approximately)
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
REPLACE INTO `bookings` (`booking_id`, `user_id`, `vehicle_id`, `date`) VALUES
	(1, 1, 1, '2022-04-03 14:43:24');
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;

-- Dumping data for table smart-garage.bookings_services: ~3 rows (approximately)
/*!40000 ALTER TABLE `bookings_services` DISABLE KEYS */;
REPLACE INTO `bookings_services` (`service_id`, `booking_id`, `service_type_id`, `price`) VALUES
	(5, 1, 1, 25),
	(6, 1, 2, 0),
	(7, 1, 3, 0);
/*!40000 ALTER TABLE `bookings_services` ENABLE KEYS */;

-- Dumping data for table smart-garage.brands: ~0 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
REPLACE INTO `brands` (`brand_id`, `name`) VALUES
	(1, 'Marka');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping data for table smart-garage.manufacturers: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
REPLACE INTO `manufacturers` (`manufacturer_id`, `name`) VALUES
	(1, 'Proizvoditel');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;

-- Dumping data for table smart-garage.models: ~0 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
REPLACE INTO `models` (`model_id`, `name`) VALUES
	(1, 'Model');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping data for table smart-garage.password_reset_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_reset_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_token` ENABLE KEYS */;

-- Dumping data for table smart-garage.service_categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `service_categories` DISABLE KEYS */;
REPLACE INTO `service_categories` (`category_id`, `name`) VALUES
	(1, 'General Maintenance'),
	(2, 'Tire Services'),
	(3, 'Transmission Services'),
	(4, 'Engine Services'),
	(5, 'General Services');
/*!40000 ALTER TABLE `service_categories` ENABLE KEYS */;

-- Dumping data for table smart-garage.service_types: ~48 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
REPLACE INTO `service_types` (`service_type_id`, `name`, `category_id`) VALUES
	(1, 'Oil Changes', 1),
	(2, 'Tune Ups', 1),
	(3, 'Filter Replacements', 1),
	(4, 'Safety & Emissions Inspections', 1),
	(5, 'Windshield Wiper Blades', 1),
	(6, 'Fluid Services', 1),
	(7, 'Trip Inspections', 1),
	(8, 'Maintenance Inspections', 1),
	(9, 'Check Engine Light Diagnostics', 1),
	(10, 'Tire Rotation', 2),
	(11, 'Tire Mounting', 2),
	(12, 'Tire Balancing', 2),
	(13, 'Flat Tire Repair', 2),
	(14, 'Tire Replacement', 2),
	(15, 'Wheel Alignment', 2),
	(16, 'Wheel Repair & Replacement', 2),
	(17, 'Transmission Service & Repair', 3),
	(18, 'New & Rebuilt Transmissions', 3),
	(19, 'Driveline Maintenance & Repair', 3),
	(20, 'Axle Replacement', 3),
	(21, 'Differential Service & Repair', 3),
	(22, 'Clutch Repair & Replacement', 3),
	(23, 'Engine Repair', 4),
	(24, 'Engine Replacement', 4),
	(25, 'Engine Performance Check', 4),
	(26, 'Belt & Hose Replacement', 4),
	(27, 'Drivability Diagnostics & Repair', 4),
	(28, 'Fuel Injection Service & Repair', 4),
	(29, 'Fuel System Maintenance & Repair', 4),
	(30, 'Ignition System Maintenance & Repair', 4),
	(31, 'Electronic Troubleshooting', 5),
	(32, 'Computer Diagnostics', 5),
	(33, 'Air Conditioning Service & Repair', 5),
	(34, 'Electrical System Diagnosis & Repair', 5),
	(35, 'Brake Repair', 5),
	(36, 'Suspension & Steering Repair', 5),
	(37, 'Shocks & Struts', 5),
	(38, 'Cooling System Service & Repair\n', 5),
	(39, 'Exhaust Systems & Mufflers', 5),
	(40, 'Custom Exhaust', 5),
	(41, 'Pre-Purchase Inspections', 5),
	(42, 'Brake & Lamp Inspections', 5),
	(43, 'Fleet Services', 5),
	(44, 'Hybrid Services', 5),
	(45, 'Chassis & Suspension', 5),
	(46, 'Power Steering Repair', 5),
	(47, 'Body & Trim Repairs (Mirrors, Door Handles, Locks)', 5),
	(48, 'Power Accessory Repair', 5);
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping data for table smart-garage.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`user_id`, `username`, `email`, `password`, `phone_number`, `role_id`) VALUES
	(1, 'user', 'user@email.com', 'user', '0085585885', 1),
	(2, 'admin', 'admin@email.com', 'admin', '0888888888', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table smart-garage.user_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
REPLACE INTO `user_roles` (`role_id`, `name`) VALUES
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping data for table smart-garage.vehicles: ~2 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
REPLACE INTO `vehicles` (`vehicle_id`, `model_id`, `brand_id`, `owner_id`, `license_plate`, `vin`, `year_of_creation`) VALUES
	(1, 1, 1, 1, 'AB1234AB', '12345678901234567', '2022-04-03 14:43:00'),
	(3, 1, 1, 2, 'CA1234BB', 'JH4DA9440NS003801', '2022-03-16 00:00:00');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
